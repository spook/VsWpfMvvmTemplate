﻿namespace MyProject.BusinessLogic.Services.Paths
{
    public interface IPathService
    {
        string AppDataPath { get; }
    }
}