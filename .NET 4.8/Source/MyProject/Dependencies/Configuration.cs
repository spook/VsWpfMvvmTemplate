﻿using MyProject.BusinessLogic.Services.Dialogs;
using MyProject.BusinessLogic.Services.Messaging;
using MyProject.Services.DialogService;
using MyProject.Services.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace MyProject.Dependencies
{
    public static class Configuration
    {
        private static bool configured = false;

        public static void Configure(IUnityContainer container)
        {
            if (configured)
                return;

            container.RegisterType<IMessagingService, MessagingService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IDialogService, DialogService>(new ContainerControlledLifetimeManager());

            MyProject.BusinessLogic.Dependencies.Configuration.Configure(container);

            configured = true;
        }
    }
}
