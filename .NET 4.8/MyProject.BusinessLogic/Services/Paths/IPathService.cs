﻿namespace $safeprojectname$.Services.Paths
{
    public interface IPathService
    {
        string AppDataPath { get; }
    }
}