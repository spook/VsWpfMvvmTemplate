﻿using $safeprojectname$.BusinessLogic.Services.Dialogs;
using $safeprojectname$.BusinessLogic.Services.Messaging;
using $safeprojectname$.Services.DialogService;
using $safeprojectname$.Services.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace $safeprojectname$.Dependencies
{
    public static class Configuration
    {
        private static bool configured = false;

        public static void Configure(IUnityContainer container)
        {
            if (configured)
                return;

            container.RegisterType<IMessagingService, MessagingService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IDialogService, DialogService>(new ContainerControlledLifetimeManager());

            $safeprojectname$.BusinessLogic.Dependencies.Configuration.Configure(container);

            configured = true;
        }
    }
}
