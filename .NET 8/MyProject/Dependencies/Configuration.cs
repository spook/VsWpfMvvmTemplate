﻿using Autofac;
using $safeprojectname$.BusinessLogic.Services.Dialogs;
using $safeprojectname$.BusinessLogic.Services.Messaging;
using $safeprojectname$.Services.Dialogs;
using $safeprojectname$.Services.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace $safeprojectname$.Dependencies
{
    public static class Configuration
    {
        private static bool configured = false;

        public static void Configure(ContainerBuilder builder)
        {
            if (configured)
                return;

            builder.RegisterType<MessagingService>().As<IMessagingService>().SingleInstance();
            builder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();

            $safeprojectname$.BusinessLogic.Dependencies.Configuration.Configure(builder);

            configured = true;
        }
    }
}
