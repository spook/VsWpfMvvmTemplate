﻿using Autofac;
using MyProject.BusinessLogic.Services.Dialogs;
using MyProject.BusinessLogic.Services.Messaging;
using MyProject.Services.Dialogs;
using MyProject.Services.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProject.Dependencies
{
    public static class Configuration
    {
        private static bool configured = false;

        public static void Configure(ContainerBuilder builder)
        {
            if (configured)
                return;

            builder.RegisterType<MessagingService>().As<IMessagingService>().SingleInstance();
            builder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();

            MyProject.BusinessLogic.Dependencies.Configuration.Configure(builder);

            configured = true;
        }
    }
}
